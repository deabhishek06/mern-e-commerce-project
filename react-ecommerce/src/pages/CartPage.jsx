import Button from "../components/Button";
import Cart from "../features/cart/Cart";

const CartPage = () => {
  return (
    <div className="my-6">
      <Cart>
        <Button redirect='/checkout' name='Checkout' />
      </Cart>
    </div>
  );
};

export default CartPage;

import baseApi from "../../app/axios";

export function fetchAllProducts() {
  return new Promise(async (resolve) => {
    const response = await baseApi.get("/products");
    resolve(response.data);
  });
}

export function fetchProductsByFilter(filter) {
  let queryString = "";
  for (let key in filter) {
    queryString += `${key}=${filter[key]}&`;
  }
  console.log(queryString)

  return new Promise(async (resolve) => {
    const response = await baseApi.get("/products?" + queryString);
    resolve(response.data);
  });
}

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchAllProducts, fetchProductsByFilter } from "./productApi";

const initialState = {
  loading: false,
  products: [],
  error: "",
};

export const fetchAllProductsAnync = createAsyncThunk(
  "product/fetchAllProducts",
  async () => {
    const response = await fetchAllProducts();
    return response;
  }
);

export const fetchProductsByFilterAsync = createAsyncThunk(
  "product/fetchProductsByFilter",
  async (filter) => {
    const response = await fetchProductsByFilter(filter);
    return response;
  }
);

const productSlice = createSlice({
  name: "product",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchAllProductsAnync.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchAllProductsAnync.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.products = payload;
      state.error = "";
    });
    builder.addCase(fetchAllProductsAnync.rejected, (state) => {
      state.loading = false;
      state.products = [];
      state.error = "There is an error";
    });
    builder.addCase(fetchProductsByFilterAsync.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(
      fetchProductsByFilterAsync.fulfilled,
      (state, { payload }) => {
        state.loading = false;
        state.products = payload;
        state.error = "";
      }
    );
    builder.addCase(fetchProductsByFilterAsync.rejected, (state) => {
      state.loading = false;
      state.products = [];
      state.error = "There is an error";
    });
  },
});

export default productSlice.reducer;

import React from "react";
import { Link } from "react-router-dom";

const Button = ({ redirect, name }) => {
  return (
    <Link
      to={redirect}
      className="flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-indigo-700"
    >
      {name}
    </Link>
  );
};

export default Button;
